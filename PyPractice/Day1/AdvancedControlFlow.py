age = int(input("how old are you?"))
##if age >= 16 and age <= 65:
if 16 <= age <= 65:
    print("Have a good day at work!")

#if(age < 16) or (age > 65):
#    print("Enjoy your free time")
#else:
#    print("Have a good day at work")

## validate input
#x = "false"
#if x:
#    print("x is true")

#x = input("Please enter something")
#if x:
#    print("you have entered: {0}".format(x))
#else:
#    print("you have entered nothing")


# not operator
#print(not False)
#print(not True)

#age = int(input("Please enter your age"))
#if not(age < 18):
#    print("You are eligible for vote")
#    print("Please put x in the box")
#else:
#    print("come back in the {0}".format(18 - age))

# in operator

#parrot = "Norwegian Parrot"
#letter = input("Enter a character: ")
#if(letter in parrot):
#    print("give me an {}, Bob".format(letter))
#else:
#    print("I don't need that letter")

#not in operator

#parrot = "Norwegian Parrot"
#letter = input("Enter a character: ")
#if(letter not in parrot):
#    print("I don't need that letter")
#else:
#    print("give me an {}, Bob".format(letter))
    