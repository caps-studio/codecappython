parrot = "Norwegian Blue"
print(parrot)
print(parrot[0])
print(parrot[3])

#if the index is more than length then it will create an error.
#print(parrot[14])
print(parrot[-1])
print(parrot[0:6])
print(parrot[:6])
print(parrot[6:])
print(parrot[-4:-2])
print(parrot[-14:-4])
print(parrot[0:6:2])

number = "2,999,444,565,445,669,545,456"
print(number[1::4])
numbers = "1, 2, 3, 4, 5, 6, 7, 8, 9"
print(numbers[0::3])

string1 = "he's "
string2 = "probably "
print(string1 + string2)
print("Hello " * 5)
print("Hello " * (5 + 4))

# 'in' is case sensitive
today = "friday"
print("day" in today)
print("Fri" in today)